from nltk import *
import random

def load_text(path):
    file = open(path)
    return file.read()

def get_text_sentences(text):
    lines = text.split("\n")
    text_sentences = [sentence.strip() for line in lines for sentence in line.split(".") if sentence.strip()]
    return [word_tokenize(text_sentence) for text_sentence in text_sentences]

def get_sentences(text_sentences):
    return [pos_tag(text_sentence) for text_sentence in text_sentences]

def get_tag_sentences(sentences):
    return [[tag for (text, tag) in sentence] for sentence in sentences]

def get_vocabulary(sentences):
    return [element for sentence in sentences for element in sentence]

def get_tag_lookup(vocabulary):
    tag_lookups = {}
    for element in vocabulary:
        _, tag = element
        tag_lookups.setdefault(tag, []).append(element)
    return tag_lookups

def get_trigram(elements, index):
    left = None if index == 0 else elements[index-1]
    right = None if index+1 == len(elements) else elements[index+1]
    middle = elements[index]
    return (left, middle, right)

def get_element_tag(element):
    if element is None:
        return None
    text, tag = element
    return tag

def create_trigram_lookup(element_lists, get_element_key = None):
    trigram_lookup = {}
    for element_list in element_lists:
        for i in range(len(element_list)):
            left, middle, right = get_trigram(element_list, i)
            left_key = get_element_key(left) if get_element_key else left
            right_key = get_element_key(right) if get_element_key else right
            trigram_lookup.setdefault(left_key, {}).setdefault(right_key, []).append(middle)
    return trigram_lookup

def mutate_by_elements(sentence, elements, index):
    if elements:
        sentence = [*sentence]
        sentence[index] = random.choice(elements)
    return sentence

def mutate_by_tag(sentence, tag_lookup):
    i = random.randint(0, len(sentence) - 1)
    _, tag = sentence[i]
    choices = tag_lookup[tag]
    return mutate_by_elements(sentence, choices, i)

def mutate_by_trigram(sentence, trigram_lookup, get_element_key = None):
    i = random.randint(0, len(sentence) - 1)
    left, _, right = get_trigram(sentence, i)
    left_key = get_element_key(left) if get_element_key else left
    right_key = get_element_key(right) if get_element_key else right
    choices = trigram_lookup.setdefault(left_key, {}).setdefault(right_key, [])
    return mutate_by_elements(sentence, choices, i)

def mutate(sentence, trigram_lookup, times=1, get_element_key = None):
    while times > 0:
        i = random.randint(0, len(sentence) - 1)
        left, _, right = get_trigram(sentence, i)
        left_key = get_element_key(left) if get_element_key else left
        right_key = get_element_key(right) if get_element_key else right
        choices = trigram_lookup.setdefault(left_key, {}).setdefault(right_key, [])
        if choices:
            sentence = [*sentence]
            sentence[i] = random.choice(choices)
        times = times - 1
    return sentence

def render(*sentences):
    return ". ".join(" ".join(text for (text, tag) in sentence).replace(" ,", ",").replace(" ;", ";") for sentence in sentences) + "."

text = load_text("texts/art_of_war.txt")
text_sentences = get_text_sentences(text)
sentences = get_sentences(text_sentences)
tag_sentences = get_tag_sentences(sentences)
vocabulary = get_vocabulary(sentences)
tag_lookup = get_tag_lookup(vocabulary)
trigram_lookup = create_trigram_lookup(sentences)
tag_trigram_lookup = create_trigram_lookup(sentences, get_element_tag)

def test_mutate(sentence):
    # sentence = mutate_by_trigram(sentence, trigram_lookup)
    # sentence = mutate_by_trigram(sentence, tag_trigram_lookup, get_element_tag)
    sentence = mutate_by_tag(sentence, tag_lookup)
    sentence = mutate_by_tag(sentence, tag_lookup)
    sentence = mutate_by_tag(sentence, tag_lookup)
    sentence = mutate_by_tag(sentence, tag_lookup)
    sentence = mutate_by_tag(sentence, tag_lookup)
    sentence = mutate_by_tag(sentence, tag_lookup)
    sentence = mutate_by_tag(sentence, tag_lookup)
    sentence = mutate_by_tag(sentence, tag_lookup)
    sentence = mutate_by_tag(sentence, tag_lookup)
    return sentence

def test_part():
    return test_mutate(random.choice(sentences))

def test():
    return render(test_part(), test_part(), test_part(), test_part())
